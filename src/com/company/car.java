package com.company;

public class car {

    private double odometer;
    private String brand;
    private String color;
    private int gear=0;


    public car(double d, String b, String c){
        odometer = d;
        brand = b;
        color = c;

    }

    public car(){
        odometer = 0.;
        brand = "Renault";
        color = "white";
        gear = 0;
    }

    public car(String b, String c){
        odometer = 0.;
        brand = b;
        color = c;
        gear = 0;
    }

    public car(String b){
        odometer = 0.;
        brand = b;
        color = "White";
        gear = 0;
    }

    public void incrementGear(){
        gear += 1;
        if(gear>5){
            System.out.println("You cannot shift up more");
            gear=5;
        }
    }

    public void decrementGear(){

        gear -= 1;
        if(gear<0){
            System.out.println("You cannot shift down more");
            gear=1;
        }
    }

    public void drive(double hours, double kmTraveledPerHour){
        odometer += hours*kmTraveledPerHour;
    }

    public double getOdoMeter(){

        return this.odometer;
    }

    public void setOdoMeter(double odometer){

        this.odometer = odometer;
    }

    public String getBrand(){

        return this.brand;
    }

    public void setBrand(String brand){

        this.brand = brand;
    }

    public String getColor(){

        return this.color;
    }

    public void setColor(String color){

        this.color = color;
    }

    public int getGear(){

        return this.gear;
    }

    public void display(){
        System.out.println("Car's brand is: "+ getBrand()+ "\n" + "Car's color is: "+ getColor()+ "\n" +
                "Car's odometer is: "+ getOdoMeter()+ "\n"+ "Car's gear is: "+getGear());
        System.out.println("---------------------------");
    }





}
