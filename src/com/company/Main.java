package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        car M3 = new car(50000, "BMW", "Black");

        M3.display();
        M3.incrementGear();
        M3.display();
        M3.incrementGear();
        M3.display();
        M3.incrementGear();
        M3.display();
        M3.decrementGear();
        M3.display();
        M3.incrementGear();
        M3.incrementGear();
        M3.incrementGear();
        M3.incrementGear();
        M3.incrementGear();
        M3.incrementGear();
        M3.display();
    }
}
